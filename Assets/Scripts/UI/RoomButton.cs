﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class RoomButton : MonoBehaviour
    {
        public Text NameText;
        private Action<string> onClicked;
        private string roomName;

        public void Initialize(string roomName, Action<string> onClicked)
        {
            this.roomName = roomName;
            this.onClicked = onClicked;
            NameText.text = roomName;
        }

        public void OnClick() => onClicked(roomName);
    }
}
