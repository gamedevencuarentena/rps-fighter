﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class PlayerInputIcon : MonoBehaviour
    {
        public Image Icon;
        public FightActionIcon[] ActionIcons;

        public void Initialize(FightActionType actionType) => Icon.sprite = InputIconFromAction(actionType);

        private Sprite InputIconFromAction(FightActionType actionType) => 
            ActionIcons.First(ai => ai.FightActionType == actionType).SpriteIcon;
    }

    [Serializable]
    public class FightActionIcon
    {
        public FightActionType FightActionType;
        public Sprite SpriteIcon;
    }
}
