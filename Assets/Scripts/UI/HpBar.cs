﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class HpBar : MonoBehaviour
    {
        public Image FillBar;
        public void UpdateValue(float value) => FillBar.fillAmount = value;
    }
}