﻿

namespace Assets.Scripts.Core
{
    public class TurnResolver
    {
        public static TurnResult Resolve(FightAction fightAction1, FightAction fightAction2)
        {
            if(ShouldTie(fightAction1, fightAction2))
                return TurnResult.CreateTie();

            if (Blocking(fightAction2, fightAction1))
                return TurnResult.CreateAttack1Blocked();

            if (Blocking(fightAction1, fightAction2))
                return TurnResult.CreateAttack2Blocked();

            return fightAction1.Beats(fightAction2)
                ? TurnResult.CreateAction1Win()
                : TurnResult.CreateAction2Win();
        }

        static bool Blocking(FightAction blockingAction, FightAction other) =>
            blockingAction.ActionType == FightActionType.Block && other.IsAnAttack;

        static bool ShouldTie(FightAction fightAction1, FightAction fightAction2)
        {
            if (fightAction1.AttacksAreEqual(fightAction2))
                return true;

            if (!fightAction1.IsAnAttack && !fightAction2.IsAnAttack)
                return true;

            return false;
        }
    }
}