﻿namespace Assets.Scripts.Core
{
    public enum FightActionType
    {
        None,
        Punch,
        Kick,
        Headbutt,
        Amplify,
        Block
    }
}