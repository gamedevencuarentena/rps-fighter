﻿using Assets.Scripts.Core;
using Assets.Scripts.Player;
using Photon.Pun;

namespace Assets.Scripts.Networking
{
    public class RemoteNetworkInput : PlayerInput
    {
        [PunRPC]
        public void ReceiveInput(FightActionType inputAction) => 
            QueFightAction(inputAction);
    }
}
