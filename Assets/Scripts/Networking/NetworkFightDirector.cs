﻿using System.Linq;
using Assets.Scripts.Directors;
using Photon.Pun;
using UnityEngine.UI;

namespace Assets.Scripts.Networking
{
    public class NetworkFightDirector : FightDirector
    {
        public LocalNetworkInput LocalInput;
        public RemoteNetworkInput RemoteInput;
        public Text Player1Name;
        public Text Player2Name;

        protected override void Start()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                LocalInput.PlayerId = 0;
                RemoteInput.PlayerId = 1;
                
                Player1.PlayerInput = LocalInput;
                Player2.PlayerInput = RemoteInput;
            }
            else
            {
                LocalInput.PlayerId = 1;
                RemoteInput.PlayerId = 0;

                Player1.PlayerInput = RemoteInput;
                Player2.PlayerInput = LocalInput;
            }

            Player1Name.text = PhotonNetwork.PlayerList.First().NickName;
            Player2Name.text = PhotonNetwork.PlayerList.Last().NickName;

            base.Start();
        }
    }
}
