﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Menu;
using Photon.Pun;
using Photon.Realtime;

namespace Assets.Scripts.Networking
{
    public class NetworkMenuManager : MonoBehaviourPunCallbacks
    {
        MultiplayerMenu multiplayerMenu;

        public string RoomName =>
            string.Format("{0}-{1}",
                PhotonNetwork.LocalPlayer.NickName,
                string.Join(string.Empty, Guid.NewGuid().ToString().Take(5)));

        public void ConnectToMaster(MultiplayerMenu multiplayerMenu)
        {
            this.multiplayerMenu = multiplayerMenu;
            PhotonNetwork.AutomaticallySyncScene = true;
            PhotonNetwork.ConnectUsingSettings();
        }

        public void CreateRoom()
        {
            var defaultRoomOptions = new RoomOptions { MaxPlayers = 2 };
            PhotonNetwork.CreateRoom(RoomName, defaultRoomOptions);
        }

        public void JoinRoom(string roomName) => PhotonNetwork.JoinRoom(roomName);

        public override void OnRoomListUpdate(List<RoomInfo> roomList) => 
            multiplayerMenu.UpdateRoomList(roomList);

        public override void OnConnectedToMaster()
        {
            base.OnConnectedToMaster();
            PhotonNetwork.JoinLobby(TypedLobby.Default);
            PhotonNetwork.LocalPlayer.NickName = DataManager.GetPlayerName();
            multiplayerMenu.OnConnect();
        }

        public override void OnCreatedRoom()
        {
            base.OnCreatedRoom();
            multiplayerMenu.ShowRoom();
            UpdatePlayerList();
        }

        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();
            multiplayerMenu.ShowRoom();
            UpdatePlayerList();
        }

        public override void OnPlayerEnteredRoom(Photon.Realtime.Player newPlayer)
        {
            base.OnPlayerEnteredRoom(newPlayer);
            UpdatePlayerList();
        }

        public override void OnPlayerLeftRoom(Photon.Realtime.Player otherPlayer)
        {
            base.OnPlayerLeftRoom(otherPlayer);
            UpdatePlayerList();
        }

        void UpdatePlayerList() => 
            multiplayerMenu.ShowPlayers(PhotonNetwork.CurrentRoom.Players.Values.Select(p => p.NickName));
    }
}
