﻿using Assets.Scripts.Core;
using Assets.Scripts.Player;
using Photon.Pun;
using UnityEngine;

namespace Assets.Scripts.Networking
{
    public class LocalNetworkInput : PlayerInput
    {
        public PhotonView PhotonView;

        void Update() => ReadInput();

        private void ReadInput()
        {
            if (Input.GetButtonDown($"Punch0"))
                ReadInput(FightActionType.Punch);
            if (Input.GetButtonDown($"Kick0"))
                ReadInput(FightActionType.Kick);
            if (Input.GetButtonDown($"Headbutt0"))
                ReadInput(FightActionType.Headbutt);
            if (Input.GetButtonDown($"Amplify0"))
                ReadInput(FightActionType.Amplify);
            if (Input.GetButtonDown($"Block0"))
                ReadInput(FightActionType.Block);
        }

        private void ReadInput(FightActionType action)
        {
            PhotonView.RPC("ReceiveInput", RpcTarget.Others, action);
            QueFightAction(action);
        }
    }
}
