﻿using System;
using Assets.Scripts.Core;
using Assets.Scripts.Player;

namespace Assets.Scripts.Presentation
{
    public class Turn
    {
        readonly PlayingCharacter player1;
        readonly PlayingCharacter player2;

        public Turn(PlayingCharacter player1, PlayingCharacter player2)
        {
            this.player1 = player1;
            this.player2 = player2;
        }

        public TurnResultEffect Resolve(FightAction action1, FightAction action2, Action shakeCamera)
        {
            var turnResult = TurnResolver.Resolve(action1, action2);
            var damageP1 = player1.IsAmplified ? 20 : 10;
            var damageP2 = player2.IsAmplified ? 20 : 10;
            ResumeAnimators();

            if (turnResult.Attack1Wins)
            {
                player2.ReceiveDamage(damageP1);
                PlayAnimations(PlayerAttack(action1), player2.IsKnockedOut ? "KnockOut" : "GetHit");

                if (player1.IsAmplified)
                    shakeCamera();

                player1.SpendAmplify();
            }

            if (turnResult.Attack2Wins)
            {
                player1.ReceiveDamage(damageP2);
                PlayAnimations(player1.IsKnockedOut ? "KnockOut" : "GetHit", PlayerAttack(action2));
                
                if (player2.IsAmplified)
                    shakeCamera();

                player2.SpendAmplify();
            }

            if (turnResult.Attack1Blocked)
            {
                player2.ReceiveDamage(damageP1/2);
                PlayAnimations(PlayerAttack(action1), player2.IsKnockedOut ? "KnockOut" : "Block");

                if (player1.IsAmplified)
                    shakeCamera();

                player1.SpendAmplify();
                return TurnResultEffect.Attack1Blocked;
            }

            if (turnResult.Attack2Blocked)
            {
                player1.ReceiveDamage(damageP2/2);
                PlayAnimations(player1.IsKnockedOut ? "KnockOut" : "Block", PlayerAttack(action2));

                if (player2.IsAmplified)
                    shakeCamera();

                player2.SpendAmplify();
                return TurnResultEffect.Attack2Blocked;
            }

            if (turnResult.IsATie)
            {
                FreezeAnimators();
                PlayAnimations(action1.ActionType.ToString(), action2.ActionType.ToString());

                if (action1.ActionType==FightActionType.Amplify)
                    player1.Amplify();

                if (action2.ActionType == FightActionType.Amplify)
                    player2.Amplify();
            }

            if (player1.IsKnockedOut)
            {
                player1.CharacterView.PlayAnimation("KnockOut");
                return TurnResultEffect.Player1KnockedOut;
            }

            if (player2.IsKnockedOut)
            {
                player2.CharacterView.PlayAnimation("KnockOut");
                return TurnResultEffect.Player2KnockedOut;
            }

            return TurnResultEffect.None;
        }

        private string PlayerAttack(FightAction action) => 
            action.IsAmplified ? action.ActionType + "Super" : action.ActionType.ToString();

        private void ResumeAnimators()
        {
            player1.CharacterView.Resume();
            player2.CharacterView.Resume();
        }

        private void FreezeAnimators()
        {
            player1.CharacterView.StopInPlace();
            player2.CharacterView.StopInPlace();
        }

        private void PlayAnimations(string tag1, string tag2)
        {
            player1.CharacterView.PlayAnimation(tag1);
            player2.CharacterView.PlayAnimation(tag2);
        }
    }

    public enum TurnResultEffect
    {
        None,
        Attack1Blocked,
        Attack2Blocked,
        Player1KnockedOut,
        Player2KnockedOut
    }
}
