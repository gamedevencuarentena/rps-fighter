﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Presentation
{
    public class CameraShake : MonoBehaviour
    {
        public float Delay;
        public float Duration;
        public float Violence;

        public void Shake() => StartCoroutine(ShakeCamera());

        private IEnumerator ShakeCamera()
        {
            var elapsed = Duration;
            var camera = Camera.main;
            var initialCameraPosition = camera.transform.position;

            while (elapsed > 0)
            {
                if (elapsed < Duration - Delay)
                {
                    var shakeX = Random.Range(-Violence,Violence);
                    camera.transform.position += new Vector3(shakeX, 0f, 0f);
                }

                elapsed -= Time.deltaTime;
                yield return null;
            }

            camera.transform.position = initialCameraPosition;
            
        }
    }
}
