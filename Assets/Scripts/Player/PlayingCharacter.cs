﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Directors;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class PlayingCharacter : MonoBehaviour
    {
        public int StartingHp;
        public PlayerInput PlayerInput;
        public CharacterView CharacterView;
        public ActiveActionIcon ActiveActionIcon;
        public HpBar HpBar;
        public Animator Animator;

        private int currentHp;

        public int PlayerId => PlayerInput.PlayerId;

        public void InitializeAsHuman(RoundDirector roundDirector) => Initialize(roundDirector);
        public void InitializeAsAi(RoundDirector roundDirector)
        {
            PlayerInput.Disable();
            PlayerInput = GetComponent<AIPlayerInput>();
            Initialize(roundDirector);
        }

        void Initialize(RoundDirector roundDirector)
        {
            currentHp = StartingHp;
            PlayerInput.Initialize(roundDirector);
            CharacterView.Initialize(StartingHp, HpBar, Animator);
        }

        public bool IsAmplified { get; private set; }
        public bool IsKnockedOut => currentHp <= 0;

        public void ReceiveDamage(int amount)
        {
            currentHp -= amount;
            CharacterView.UpdateHp(currentHp);
        }

        public void Amplify() => 
            IsAmplified = true;

        public void SpendAmplify() => IsAmplified = false;
    }
}
