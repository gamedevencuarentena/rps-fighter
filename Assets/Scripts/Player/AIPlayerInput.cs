﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Core;
using Assets.Scripts.Directors;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class AIPlayerInput : PlayerInput
    {
        IEnumerable<FightActionType> possibleActions = new[]
        {FightActionType.Punch, FightActionType.Kick, FightActionType.Headbutt, FightActionType.Block, FightActionType.Amplify};

        public void PlayRandomRound()
        {
            var actions = new List<FightActionType>();

            foreach (var _ in Enumerable.Range(0, 5))
                actions.Add(possibleActions.PickOne());

            foreach (var action in actions)
                QueFightAction(action);
        }

        public override void Enable() => PlayRandomRound();
    }
}
