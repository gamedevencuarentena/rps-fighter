﻿using UnityEngine;

namespace Assets.Scripts.Audio
{
    public class CharacterAudio : MonoBehaviour
    {
        public AudioSource PunchSource;
        public AudioSource WooshSource;
        public AudioSource VoiceSource;
        public AudioLibrary AudioLibrary;

        public void PlayClip(ClipType clipType)
        {
            var clip = AudioLibrary.GetRandomClip(clipType);
            var source = SelectSource(clipType);

            source.clip = clip;
            source.Play();
        }

        private AudioSource SelectSource(ClipType clipType)
        {
            if (clipType == ClipType.Punch)
                return PunchSource;
            if (clipType == ClipType.Woosh)
                return WooshSource;
            return VoiceSource;
        }
    }
}
